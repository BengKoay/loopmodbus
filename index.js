const ModbusRTU = require('modbus-serial');
let client = new ModbusRTU();
// client.connectRTUBuffered(
//   '/dev/ttyAMA0',
//   { baudRate: 9600 },
//   function (error, success) {
//     if (error) {
//       console.log('Serial Port initialization unsuccessful');
//     } else {
//       console.log('Serial port initialization successful');
//     }
//   }
// );

client.connectTCP('192.168.50.181', { port: 502 }, function (error, data) {
	if (error) {
		console.log('TCP/IP port initialization unsuccessful');
	} else {
		console.log('TCP/IP port initialization successful');
	}
});
// client.setTimeout(500); // setTimeout only for RTU

// SP 1 = 79, SP 2 = 93

var a = 0;
var success = 0;
var loss = 0;
var output = [],
	n,
	padded;

function sleep(ms) {
	return new Promise((resolve) => {
		setTimeout(resolve, ms);
	});
}
var i = 0;
const main = async () => {
	// for (var i = 1; i <= 255; i++) {
	await client.setID(parseInt(i));
	console.log('client.setID i', i);
	for (n = 0; n <= 9999; n += 10) {
		// from 0000 to 9999
		padded = ('000' + n).slice(-4); // Prefix three zeros, and get the last 4 chars
		output.push(padded);
	}
	// console.log(output);

	for (const eachOutput of output) {
		await client.readInputRegisters(eachOutput, 10, async function (err, data) {
			//   process.stdout.write('.');
			if (!!data) {
				console.log('eachOutput', eachOutput);

				console.log(data);
				if (err != null || err != 'null') {
					success += 1;
				} else {
					loss += 1;
				}
				console.log('Total Packet: ', (a = success + loss));
				console.log('Successful Packet: ', success);
				console.log('Unsuccessful Packet: ', loss);
			}
		});
		// console.log('eachOutput', eachOutput);
		await sleep(10);
	}
	console.log('end');
	// }
};

main();
